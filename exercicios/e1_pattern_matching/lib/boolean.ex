defmodule Boolean do

	def b_not(a) do
		case a do
			true -> false
			false -> true
		end
	end

	def b_and(a,b) do
		case {a,b} do
			{false, false} -> false
			{false, true} -> false
			{true, false} -> false
			{true, true} -> true
			
		end
	end

	def b_or(a,b) do
		case {a,b} do
			{false, false} -> false
			{false, true} -> true
			{true, false} -> true
			{true, true} -> true
		end
	end

end
