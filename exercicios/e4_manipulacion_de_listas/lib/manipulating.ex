defmodule Manipulating do

	defp aux_filter(l,n,acc) do
		case l do
			[] -> acc
			[h|t] -> if (h<=n) do aux_filter(t,n,[h|acc])
					 else aux_filter(t,n,acc)
					 end
		end
	end
	def filter(l,n) do 
		l |> aux_filter(n,[]) |> reverse()
	end

	defp aux_reverse(l,acc) do
		case l do
			[] -> acc
			[h|t] -> aux_reverse(t,[h|acc])
			h -> [h|acc]
		end
	end
	def reverse(l) do 
		l |> aux_reverse([])
	end

	defp agregar(e,l) do
		case e do
			[] -> l
			[h|t] -> (agregar(t,[h|l]))
			e -> [e|l]
		end
	end

	defp conc_aux(l,acc) do
		case l do
			[] -> acc
			[h,t] -> agregar(reverse(h),conc_aux(t,acc))
			h -> [h|acc]
		end
	end
	def concatenate(l) do
		l |> conc_aux([])
	end

	defp flatten_aux(l,acc) do
		case l do
			[] -> acc
			[h,t] -> flatten_aux(t,agregar(h,acc))
			h -> flatten_aux([],agregar(h,acc))
		end
	end
	def flatten(l) do
		l |> flatten_aux([]) |> reverse()
	end

end
 