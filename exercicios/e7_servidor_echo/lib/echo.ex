defmodule Echo do

	def start() do
		spawn(fn -> loop() end) |> Process.register(:echo)
	end

	def stop() do 
		send Process.whereis(:echo), :stop	
	end

	def print(term) do
		send Process.whereis(:echo), {:print, term}
	end

	defp loop() do
		receive do
			:stop -> :ok
			{:print,msg} -> IO.puts("#{msg}")
							loop()
		end
	end
end
