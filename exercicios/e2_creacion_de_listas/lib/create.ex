defmodule Create do

	def create(n) do
		case n do
			0 -> []
			_ -> create(n-1) ++ [n]
		end
	end

	def reverse_create(n) do
		case n do
			0 -> []
			_ -> [n | reverse_create(n-1)]
		end
	end

end