defmodule Effects do
	
	def print(n) do
		case n do
			0 -> []
			_ -> print(n-1) 
				 IO.puts("#{n}")
				
		end
	end

	def even_print(n) do
		case n do
			0 -> []
			n when is_integer(n) and rem(n,2) == 0 -> even_print(n-2)
				IO.puts("#{n}")
			n when is_integer(n) and rem(n,2) == 1 -> even_print(n-1) 
		end
	end
	
end