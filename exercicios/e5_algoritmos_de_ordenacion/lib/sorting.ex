defmodule Sorting do

	#INIT - Auxiliares para evitar el uso de Enum.xxxxxxx
	defp aux_reverse(l,acc) do
		case l do
			[] -> acc
			[h|t] -> aux_reverse(t,[h|acc])
			h -> [h|acc]
		end
	end
	defp reverse(l) do 
		l |> aux_reverse([])
	end

	defp aux_filterbajo(l,n,acc) do
		case l do
			[] -> acc
			[h|t] -> if (h<n) do aux_filterbajo(t,n,[h|acc])
					 else aux_filterbajo(t,n,acc)
					 end
		end
	end
	defp filtrobajo(l,n) do
		l |> aux_filterbajo(n,[]) |> reverse()
	end

	defp aux_filteralto(l,n,acc) do
		case l do
			[] -> acc
			[h|t] -> if (h>n) do aux_filteralto(t,n,[h|acc])
					 else aux_filteralto(t,n,acc)
					 end
		end
	end
	defp filtroalto(l,n) do
		l |> aux_filteralto(n,[]) |> reverse()
	end
	#FIN - Auxiliares para evitar el uso de Enum.xxxxxxx

	def quicksort([]), do: []
	def quicksort([pivote|[]]), do: [pivote]
	def quicksort([pivote|t]) do
		bajo = filtrobajo(t,pivote)
		alto = filtroalto(t,pivote)
		quicksort(bajo) ++ [pivote] ++ quicksort(alto)
	end
	
	defp merge(p, [], r), do: r++p
	defp merge([],s,r), do: r++s
	defp merge([ph|pt], [sh|st], r) do
		if (ph<sh) do
			merge(pt, [sh|st], r++[ph])
			else merge([ph|pt], st, r ++[sh])
		end
	end

	def mergesort([]), do: []
	def mergesort([l]), do: [l]
	def mergesort(l) do
		{izda,dcha} = Enum.split(l,div(length(l),2))
		merge(mergesort(izda), mergesort(dcha),[])
	end
end
