defmodule Db do

	def new() do 
		[] 
	end

	def write(ref,k,e) do
		[{k,e}|ref]
	end

	defp aux_reverse(l,acc) do
		case l do
			[] -> acc
			[h|t] -> aux_reverse(t,[h|acc])
			h -> [h|acc]
		end
	end
	defp reverse(l) do 
		l |> aux_reverse([])
	end

	defp aux_delete([],[],acc), do: acc
	defp aux_delete([h|t],n,acc) do
		case h do
			[] -> acc
			{cl,_} -> if (cl==n) do aux_delete(t,[],acc)
					 else aux_delete(t,n,[h|acc])
					 end
			[{cl,_}] -> if (cl==n) do aux_delete(t,[],acc)
					 else aux_delete(t,n,[h|acc])
					 end

		end
	end
	def delete(l,n) do 
		l |> aux_delete(n,[]) |> reverse()
	end

	def read(l,n) do
		case l do
			[] -> {:error, :not_found}
			[{cl,e}|t] -> if (cl==n) do {:ok, e}
					 else read(t,n)
			end
		end
	end

	defp aux_match([],_,acc), do: acc
	defp aux_match([h|t],n,acc) do
		case h do
			[] -> acc
			{cl,e} -> if (e==n) do aux_match(t,n,[cl|acc])
					 else aux_match(t,n,acc)
					 end
			[{cl,e}] -> if (e==n) do aux_match(t,n,[cl|acc])
					 else aux_match(t,n,acc)
					 end

		end
	end
	def match(l,n) do 
		l |> aux_match(n,[]) |> reverse()
	end

	def destroy(l) do
		case l do
			[] -> :ok
			[{cl,_}|_] -> destroy(delete(l,cl))
		end
	end

end
